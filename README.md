# Présentation du GT MAIIA

Le succès de l’ANF "Machine learning pour la microscopie" en octobre 2021 et la participation d’un grand nombre de nos collègues aux ateliers "Deep Learning" de MiFoBio2021, ont motivé la création de ce nouveau groupe de travail "Méthodes d’Analyses d’Images par Intélligence Artificielle (MAIIA). Les objectifs du GT MAIIA sont d’accompagner notre communauté face aux défis de la révolution de l’intelligence artificielle en:

* réalisant un état des lieux des outils « clé en main » deep learning et de leur utilisation par la communauté (ZeroCostDL4Mic, DeepImagej pour en citer deux)

* rassemblant la communauté des utilisateurs (biologistes, microscopistes et bio-image analystes) et des développeurs d’outils Deep Learning sur des problématiques de microscopie

* formant nos collègues des plateformes au déploiement et à l’utilisation des outils

* réfléchissant aux bonnes pratiques d’utilisation de ces outils (publication de notes d’application, standardisation des protocoles, matériels et méthodes)

* Une liste de discussion est d’ores et déjà activée suite à l’ANF de 2021. (anfdl4mic-mfm@services.cnrs.fr). Pour vous s'inscrire à la liste de diffusion, merci de suivre cette [procèdure](https://forgemia.inra.fr/gt-maiia/GT-MAIIA/-/wikis/Inscription-liste-diffusion). 

Veuillez trouver nos actions et production [ici](https://forgemia.inra.fr/gt-maiia/GT-MAIIA/-/wikis/home)

<div align="center">
<img src="https://forgemia.inra.fr/gt-maiia/GT-MAIIA/-/wikis/illustrations/MAIIA_logo_animation.gif" width="25%" />

</div>